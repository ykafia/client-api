package com.coface.corp.drools.call.impl;

import java.util.HashSet;
import java.util.Set;

import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.KieServiceResponse;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coface.corp.drools.call.Call;

public class RemoteCall implements Call {

	private static final Logger logger = LoggerFactory.getLogger(RemoteCall.class);

	private long timeout = 10000;

	private String username = "atlas";

	private String url;

	private String password;

	private String containerId;

	private KieServicesClient client;

	public RemoteCall() {
	}

	public ExecutionResults executerCommandes(BatchExecutionCommand batchCommandes) throws Exception {
		ServiceResponse<ExecutionResults> reponse = null;

		ExecutionResults ret = null;

		RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
		
		reponse = ruleClient.executeCommandsWithResults(containerId, batchCommandes);

		if ((reponse.getType() != null) && (reponse.getType().equals(KieServiceResponse.ResponseType.SUCCESS))) {
			ret = (ExecutionResults) reponse.getResult();
		} else {
			String msg = "[ERREUR] Erreur d'appel au Server BRMS..." + reponse.getMsg();
			logger.error(msg);
			Exception remoteException = new Exception(msg);
			throw remoteException;
		}

		return ret;
	}

	
	public void postConstruct() {
		logger.info("Call : " + url + " : " + username + " : XXXXXX ");
		logger.info("ContainerId : " + containerId);

		KieServicesConfiguration config = null;
		if (timeout == -1) {
			config = KieServicesFactory.newRestConfiguration(url, username, password);
		} else {
			config = KieServicesFactory.newRestConfiguration(url, username, password, timeout);
		}
		config.setMarshallingFormat(MarshallingFormat.JSON);
		
		System.out.println(config);
		
		Set<Class<?>> clazzList= new HashSet<Class<?>>();
		clazzList.add(String.class);
		
		config.setExtraClasses(clazzList);
		client = KieServicesFactory.newKieServicesClient(config);
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getContainerId() {
		return this.containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public KieServicesClient getClient() {
		return client;
	}

	public void setClient(KieServicesClient client) {
		this.client = client;
	}

}
