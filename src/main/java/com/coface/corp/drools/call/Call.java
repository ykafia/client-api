package com.coface.corp.drools.call;

import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.runtime.ExecutionResults;

public interface Call {
	ExecutionResults executerCommandes(BatchExecutionCommand batchCommandes) throws Exception;
}

