package com.coface.corp.drools.util;

public class DroolsClientConstants {
    public  static final String RESULT = "output";
    public static final String EMPTY_PROCESSUS_ID = "com.coface.corp.drools.empty.process.executionEmptyID";
    
    public static final String DEFAULT_STATELESS_KIE_SESSION = "defaultStatelessKieSession";
	public static final int BRMS_MAX_RULES_TO_FIRE = 25000;
}