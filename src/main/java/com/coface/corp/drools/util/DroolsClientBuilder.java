package com.coface.corp.drools.util;

import com.coface.corp.drools.call.impl.RemoteCall;
import com.coface.corp.drools.client.DroolsEngineClient;


public class DroolsClientBuilder {

    public static DroolsEngineClient buildClient(String remoteUrl, String username, String password, String containerID){
        RemoteCall caller = new RemoteCall();
        caller.setUrl(remoteUrl);
        caller.setUsername(username);
        caller.setPassword(password);
        caller.setContainerId(containerID);
        caller.postConstruct();
        return new DroolsEngineClient(caller);
    }
}