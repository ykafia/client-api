package com.coface.corp.drools.client;

import java.util.ArrayList;

import org.drools.core.runtime.rule.impl.FlatQueryResults;
import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coface.corp.drools.call.Call;
import com.coface.corp.drools.util.DroolsClientConstants;
import com.coface.corp.drools.util.Engines;

public class DroolsEngineClient {
	

	private static final Logger logger = LoggerFactory.getLogger(DroolsEngineClient.class);

	private Call call;

	

	private static KieCommands cmdFactory = KieServices.Factory.get().getCommands();

	public DroolsEngineClient() {

	}
	public DroolsEngineClient(final Call caller) {
		this.call = caller;
	}


	public String executionEngine(final String in, Engines engine) throws Exception {

		logger.debug("Début execution Engine...");

		final BatchExecutionCommand batchCommandes = getCommand(in, engine);

		final ExecutionResults bresults = call.executerCommandes(batchCommandes);

		final String ret = result(bresults);

		return ret;
	}
	
	private static BatchExecutionCommand getCommand(final String inADCkb, Engines engine) {
		BatchExecutionCommand batchexecutionCommand = null;

		final ArrayList<Command> listeCommandes = new ArrayList<Command>();

		listeCommandes.add(cmdFactory.newInsert(inADCkb, DroolsClientConstants.RESULT));
		if(engine == Engines.EMPTY)
			listeCommandes.add(cmdFactory.newStartProcess(DroolsClientConstants.EMPTY_PROCESSUS_ID));
		
		
		
		listeCommandes.add(cmdFactory.newFireAllRules(DroolsClientConstants.BRMS_MAX_RULES_TO_FIRE));
		listeCommandes.add(cmdFactory.newQuery("output", "GetOutput"));
		batchexecutionCommand = cmdFactory.newBatchExecution(listeCommandes, DroolsClientConstants.DEFAULT_STATELESS_KIE_SESSION);

		return batchexecutionCommand;
	}

	private static String result(final ExecutionResults bresults) throws Exception{
		String ret = null;
		for(QueryResultsRow o : ((FlatQueryResults)bresults.getValue(DroolsClientConstants.RESULT))) {
			if(String.class.equals(o.get("output").getClass()))
				ret = (String) o.get("output");
		}
		return ret;
	}
	public Call getCall() {
		return this.call;
	}

	public void setCall(final Call call) {
		this.call = call;
	}


}
